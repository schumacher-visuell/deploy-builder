# Deploy Builder
## Description
This is a simple builder for deploying a project to a server. It is designed to be used with a CI/CD pipeline, but can be used manually as well.

## Publishing
To publish a new version, simply create a new tag and push it to the repository. The CI/CD pipeline will automatically build and publish a new version.