FROM ubuntu:jammy
ARG VERSION=latest
LABEL maintainer="Ben Kremer <hallo@benkremer.de>"  \
      version="${VERSION}"

RUN apt-get update -y \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y curl \
    && curl -fsSL https://deb.nodesource.com/setup_16.x | bash - \
    && apt-get install -y openssh-client zip unzip composer rsync nodejs php-curl libnotify-bin git \
    && npm install -g yarn \
    && composer config -g cache-dir "$(pwd)/composer_cache" \
    && composer config -g cache-files-ttl 2592000 \
    && composer config -g cache-files-maxsize "100MiB" \
    && yarn config set cache-folder .yarn \
    && mkdir -p ~/.ssh \
    && echo -e "Host * \n \tStrictHostKeyChecking no \n \n" > ~/.ssh/config \
    && node -v

